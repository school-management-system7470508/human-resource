CREATE table "Persons"
(
    id          BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    first_name  varchar(255) not null,
    middle_name varchar(255) null,
    last_name   varchar(255) not null
)