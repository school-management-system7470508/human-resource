package com.school.hr.repositories;

import com.school.hr.entities.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Long> {

    Person findByFirstNameAndLastNameAllIgnoringCase(String firstName, String lastName);
}
