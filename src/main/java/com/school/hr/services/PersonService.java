package com.school.hr.services;

import com.school.hr.entities.Person;
import com.school.hr.repositories.PersonRepository;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

import static lombok.AccessLevel.PRIVATE;

@Service
@FieldDefaults(makeFinal = true, level = PRIVATE)
@AllArgsConstructor
public class PersonService {

    PersonRepository personRepository;

    public List<Person> getPersons() {
        return this.personRepository.findAll(Pageable.ofSize(10)).toList();
    }
}
