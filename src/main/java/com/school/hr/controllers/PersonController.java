package com.school.hr.controllers;

import com.school.hr.entities.Person;
import com.school.hr.services.PersonService;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static lombok.AccessLevel.PRIVATE;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/persons")
@AllArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class PersonController {

    PersonService personService;

    @ResponseStatus(OK)
    @GetMapping
    public List<Person> persons() {
        return personService.getPersons();
    }
}
